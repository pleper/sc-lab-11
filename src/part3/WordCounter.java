package part3;

import java.util.HashMap;

public class WordCounter {
	private String message;
	private HashMap<String,Integer> wordList;
	public WordCounter(String message){
		this.message = message;
	}
	
	public void count(){
		wordList = new HashMap<String,Integer>();
		String[] words = message.split(" ");
		for (String word:words){
		
			if(wordList.containsKey(word)){
				wordList.put(word, wordList.get(word)+1);
			}else{
				wordList.put(word, 1);
			}
		}
	}
	
	public int hasWord(String word){
		if(wordList.containsKey(word)){
			return wordList.get(word);
		}else{
			return 0;
		}
		
	}
}
