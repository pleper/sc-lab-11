package part4;

public class Main {
	/*
	 * �ͺ�Ӷ����� 1 : �Դ������͵ç��÷Ѵ throw DataException(); �������
	 * DataException ��Ẻchecked exception ��� ���ʹ������¡�� ��ͧ�ա��
	 * throws ��͢�ҧ��ѧ�������ʹ���� ��� throw DataException(); ��ͧ�� throw
	 * new DataException(); ��з���÷Ѵ throws new FormatException();
	 * ��ͧ�����¹�� throw new FormatException();
	 * 		   2 �ҡ����� Exception ���� ABCFG
	 * 		   3 ADFG
	 * 		   4 ABEFG
	 */

	public static void main(String[] args) {

		try {
			MyClass c = new MyClass();
			System.out.print("A");
			c.methX();
			System.out.print("B");
			c.methY();
			System.out.print("C");
			return;
		} catch (DataException e) {
			System.out.print("D");
		} catch (FormatException e) {
			System.out.print("E");
		} finally {
			System.out.print("F");
		}
		System.out.print("G");
	}
}
