package part5;

public class Main {

	
	public static void main(String[] args) {
		Refrigerator re = new Refrigerator(3);
		try {
			re.put("Cat");
			re.put("Dog");
			re.put("Rabbit");
			System.out.println(re);
			System.out.println(re.takeOut("Dog"));
			System.out.println(re.takeOut("Bear"));
			System.out.println(re);
			re.put("Bird");
			re.put("Fish");
		} catch (FullException e) {
			System.out.println("Refrigerator is full.");
		}
	}
}
